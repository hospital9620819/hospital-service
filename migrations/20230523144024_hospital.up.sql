CREATE TABLE IF NOT EXISTS doctors {
    id SERIAL PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name NOT NULL,
    age INTEGER NOT NULL,
    specialization TEXT NOT NULL,
    gender TEXT NOT NULL,
    salary TEXT NOT NULL,
    addres TEXT NOT NULL,
    phone_number TEXT NOT NULL,
    building_id INTEGER NOT NULL,
    room_id INTEGER NOT NULL
};

CREATE TABLE IF NOT EXISTS staff {
    id SERIAL PRIMARY KEY,
    first_name TEXT NOT NULL,
    last_name NOT NULL,
    age INTEGER NOT NULL,
    position TEXT NOT NULL,
    gender TEXT NOT NULL,
    salary TEXT NOT NULL,
    addres TEXT NOT NULL,
    phone_number TEXT NOT NULL,
    building_id INTEGER NOT NULL,
    room_id INTEGER NOT NULL
};

CREATE TABLE IF NOT EXISTS rooms {
    room INTEGER,
    floor TEXT,
    empti BOOLEAN,
    price TEXT,
    descriptions TEXT NOT NULL,
    building_id INTEGER
};