package repo

import pb "hospital/hospital-service/genproto/hospital_service"

type HospitalStorageI interface {
	// Doctor...
	CreateDoctor(*pb.DoctorReq) (*pb.DoctorRes, error)
	GetDoctorByName(*pb.DoctorName) (*pb.DoctorRes, error)
	GetDoctorBySpecialization(*pb.DoctorSpecialization) (*pb.DoctorRes, error)
	UpdateDoctorInfo(*pb.UpdateDoctor) (*pb.DoctorRes, error)
	DeleteDoctor(*pb.DoctorName) (*pb.DoctorRes, error)

	// Staff...
	CreateStaff(*pb.StafReq) (*pb.StafRes, error)
	GetStafByName(*pb.StafName) (*pb.StafRes, error)
}