package storage

import (
	"hospital/hospital-service/storage/postgres"
	"hospital/hospital-service/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	Hospital() repo.HospitalStorageI
}

type storagePg struct {
	db *sqlx.DB
	hospitalRepo repo.HospitalStorageI
}

func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db: db,
		hospitalRepo: postgres.NewHospitalRepo(db),
	}
}

func (s storagePg) Hospital() repo.HospitalStorageI {
	return s.hospitalRepo
}