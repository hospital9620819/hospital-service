package postgres

import (
	pb "hospital/hospital-service/genproto/hospital_service"
	"log"
	"time"
)


// Doctors
func (d *HospitalRepo) CreateDoctor(doc *pb.DoctorReq) (*pb.DoctorRes, error) {
	var res pb.DoctorRes
	err := d.db.DB.QueryRow(`
		INSERT INTO doctors(
			first_name, last_name, age, specialization, gender,
			salary, addres, phone_number, building_id, room_id)
		VALUES(
			$1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
		RETURNING
			id,	first_name, last_name, age, specialization, gender,
			salary, addres, phone_number, building_id, room_id`,
		doc.FirstName, doc.LastName, doc.Age, doc.Specialization, doc.Gender,
		doc.Salary, doc.Addres, doc.PhoneNumber, doc.BuildingId, doc.RoomId).Scan(
			&res.Id, &res.FirstName, &res.LastName, &res.Age, &res.Specialization, &res.Gender,
			&res.Salary, &res.Addres, &res.PhoneNumber, &res.BuildingId, &res.RoomId, &res.CreatedAt)
	
	if err != nil {
		log.Println("failed create doctor info : ", err)
	}

	return &res, nil
}

func (d *HospitalRepo) GetDoctorByName(doc *pb.DoctorName) (*pb.DoctorRes, error) {
	var res pb.DoctorRes
	err := d.db.DB.QueryRow(`
		SELECT 
			id,	first_name, last_name, age, specialization, gender,
			salary, addres, phone_number, building_id, room_id
		FROM 
			doctors
		WHERE first_name = $1`, doc.DoctorName).Scan(
			&res.Id, &res.FirstName, &res.LastName, &res.Age, &res.Specialization, &res.Gender,
			&res.Salary, &res.Addres, &res.PhoneNumber, &res.BuildingId, &res.RoomId, &res.CreatedAt)

	if err != nil {
		log.Println("failed to get doctor info : ", err)
	}

	return &res, nil
}

func (d *HospitalRepo) GetDoctorBySpecialization(doc *pb.DoctorSpecialization) (*pb.DoctorRes, error) {
	var res pb.DoctorRes
	err := d.db.DB.QueryRow(`
		SELECT 
			id,	first_name, last_name, age, specialization, gender,
			salary, addres, phone_number, building_id, room_id
		FROM 
			doctors
		WHERE specialization = $1`, doc.Specialization).Scan(
			&res.Id, &res.FirstName, &res.LastName, &res.Age, &res.Specialization, &res.Gender,
			&res.Salary, &res.Addres, &res.PhoneNumber, &res.BuildingId, &res.RoomId, &res.CreatedAt)

	if err != nil {
		log.Println("failed to get doctor info by specialization : ", err)
	}

	return &res, nil
}

func (d *HospitalRepo) UpdateDoctorInfo(doc *pb.UpdateDoctor) (*pb.DoctorRes, error) {
	var res pb.DoctorRes
	err := d.db.DB.QueryRow(`
		UPDATE 
			doctors
		SET
			age = $1, salary = $2, addres = $3, phone_number = $4, room_id = $5
		WHERE 
			id = $6
		RETURNING
			id,	first_name, last_name, age, specialization, gender,
			salary, addres, phone_number, building_id, room_id`, 
			doc.Age, doc.Salary, doc.Addres, doc.PhoneNumber, doc.RoomId, doc.Id).
			Scan(
				&res.Id, &res.FirstName, &res.LastName, &res.Age, &res.Specialization, &res.Gender,
				&res.Salary, &res.Addres, &res.PhoneNumber, &res.BuildingId, &res.RoomId, &res.CreatedAt,
			)
	
	if err != nil {
		log.Println("failed to update doctor info : ", err)
	}

	return &res, nil
}

func (d *HospitalRepo) DeleteDoctor(doc *pb.DoctorName) (*pb.DoctorRes, error) {
	var res pb.DoctorRes
	err := d.db.DB.QueryRow(`
		UPDATE 
			doctors
		SET
			deleted_at = $1
		WHERE 
			doctor_name = $2
		RETURNING
			id,	first_name, last_name, age, specialization, gender,
			salary, addres, phone_number, building_id, room_id, deleted_at`,time.Now(), doc.DoctorName).
			Scan(
				&res.Id, &res.FirstName, &res.LastName, &res.Age, &res.Specialization, &res.Gender,
				&res.Salary, &res.Addres, &res.PhoneNumber, &res.BuildingId, &res.RoomId, &res.DeletedAt,
			)
	
	if err != nil {
		log.Println("failed to delete doctor : ", err)
	}

	return &res, nil
}

// Stadffs...
func(d *HospitalRepo) CreateStaff(staf *pb.StafReq) (*pb.StafRes, error) {
	return nil, nil
}

func (h *HospitalRepo) GetStafByName(staf *pb.StafName) (*pb.StafRes, error) {
	return nil,nil
}