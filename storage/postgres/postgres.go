package postgres

import (
	"github.com/jmoiron/sqlx"
)

type HospitalRepo struct {
	db *sqlx.DB
}

func NewHospitalRepo(db *sqlx.DB) *HospitalRepo {
	return &HospitalRepo{
		db: db,
	}
}