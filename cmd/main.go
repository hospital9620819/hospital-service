package main

import (
	"fmt"
	"hospital/hospital-service/config"
	db "hospital/hospital-service/pkg/db"
	"hospital/hospital-service/pkg/logger"
	"hospital/hospital-service/service"
	h 	"hospital/hospital-service/genproto/hospital_service"
	grpcclient "hospital/hospital-service/service/grpc_client"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "golang")
	defer logger.Cleanup(log)

	connDB, err := db.ConnectToDB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error: %v", logger.Error(err))
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		fmt.Println("Failed while grpc client: ", err.Error())
	}

	hospitalService := service.NewHospitalService(connDB, log, *grpcClient)

	lis, err := net.Listen("tcp", cfg.HospitalServicePort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	h.RegisterHospitalServiceServer(s, hospitalService)

	log.Info("main : servir running", logger.String("port", cfg.HospitalServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
	
}
