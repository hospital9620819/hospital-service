package config

import (
	"os"
	"github.com/spf13/cast"
)

type Config struct {
	PostgresHost       string
	PostgresPort       string
	PostgresUser       string
	PostgresDatabase   string
	PostgresPassword   string
	HospitalServiceHost string
	HospitalServicePort string
	Environment        string
	LogLevel           string
}

func Load() Config {
	c := Config{}

	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToString(getOrReturnDefault("POSTGRES_PORT", "5432"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "hospital"))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "hospital"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "h"))
	c.HospitalServiceHost = cast.ToString(getOrReturnDefault("PATIENT_SERVICE_HOST", "localhost"))
	c.HospitalServicePort = cast.ToString(getOrReturnDefault("PATIENT_SERVICE_PORT", ":7000"))
	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "developer"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
 