package service

import (
	"context"
	pb "hospital/hospital-service/genproto/hospital_service"
	l "hospital/hospital-service/pkg/logger"
	grpcClient "hospital/hospital-service/service/grpc_client"
	"hospital/hospital-service/storage"
	"log"

	"github.com/jmoiron/sqlx"
)

// Hospital Service ...
type HospitalService struct {
	storage storage.IStorage
	logger l.Logger
	Client grpcClient.GrpcClient
}

func NewHospitalService(db *sqlx.DB, log l.Logger, client grpcClient.GrpcClient) *HospitalService {
	return &HospitalService{
		storage: storage.NewStoragePg(db),
		logger: log,
		Client: client,
	}
}

// Doctors...
func (h *HospitalService) CreateDoctor(ctx context.Context, req *pb.DoctorReq) (*pb.DoctorRes, error) {
	res, err := h.storage.Hospital().CreateDoctor(req)
	if err != nil {
		log.Println("failed to create doctor info : ", err)
	}

	return res, nil
}

func (h *HospitalService) GetDoctorByName(ctx context.Context, req *pb.DoctorName) (*pb.DoctorRes, error) {
	res, err := h.storage.Hospital().GetDoctorByName(req)
	if err != nil {
		log.Println("failed to get doctor by name info : ", err)
	}

	return res, nil
}

func (h *HospitalService) GetDoctorBySpecialization(ctx context.Context, req *pb.DoctorSpecialization) (*pb.DoctorRes, error) {
	res, err := h.storage.Hospital().GetDoctorBySpecialization(req)
	if err != nil {
		log.Println("fail to get doctor by specialization : ", err)
	}

	return res, nil
}

func (h *HospitalService) UpdateDoctorInfo(ctx context.Context, req *pb.UpdateDoctor) (*pb.DoctorRes, error) {
	res, err := h.storage.Hospital().UpdateDoctorInfo(req)
	if err != nil {
		log.Println("failed to update doctor info : ", err)
	}

	return res, nil
}

func (h *HospitalService) DeleteDoctor(ctx context.Context, req *pb.DoctorName) (*pb.DoctorRes, error) {
	res, err := h.storage.Hospital().DeleteDoctor(req)
	if err != nil {
		log.Println("failed to delete doctor info : ", err)
	}

	return res, nil
}

// Staff...
func (h *HospitalService) CreateStaff(ctx context.Context, req *pb.StafReq) (*pb.StafRes, error) {
	res, err := h.storage.Hospital().CreateStaff(req)
	if err != nil {
		log.Println("failed to create staff info : ", err)
	}

	return res, nil
}

func (h *HospitalService) GetStafByName(ctx context.Context, req *pb.StafName) (*pb.StafRes, error) {
	res, err := h.storage.Hospital().GetStafByName(req)
	if err != nil {
		log.Println("failed to get staf info : ", err)
	}

	return res, nil
}
