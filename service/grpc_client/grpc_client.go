package grpcclient

import "hospital/hospital-service/config"

// gRPC client...
type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

// New...
func New(cfg config.Config) (*GrpcClient, error) {
	return &GrpcClient{
		cfg:         cfg,
		connections: map[string]interface{}{},
	}, nil
}
